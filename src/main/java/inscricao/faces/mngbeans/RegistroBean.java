/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

/**
 *
 * @author chris
 */
@Named
@ApplicationScoped
public class RegistroBean {
    ArrayList<Candidato> CandidatosList = new ArrayList<Candidato>();
    private final ListDataModel<Candidato> candidatoDataModel = new ListDataModel<>(CandidatosList);
    public void addCandidato(Candidato cand){
        CandidatosList.add(cand);
    }
    public RegistroBean(){}
    public ListDataModel<Candidato> getCandidatos(){
        return candidatoDataModel;
    }
}

